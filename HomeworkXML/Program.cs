﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace HomeworkXML
{
    class Program
    {
        public static readonly int ROUTE_ELEMENT_POSITION = 0;
        static void Main(string[] args)
        {
            // Задание №1
            List<Item> list = new List<Item>();

            XmlDocument XmlDocument = new XmlDocument();
            XmlDocument.Load("https://habr.com/ru/rss/interesting/");

            XmlNode channel = XmlDocument.GetElementsByTagName("channel")[ROUTE_ELEMENT_POSITION];

            foreach (XmlElement channelElement in channel.ChildNodes)
            {
                if (channelElement.Name == "item")
                {
                    Item item = new Item();
                    foreach (XmlElement itemElement in channelElement.ChildNodes)
                    {
                        switch (itemElement.Name)
                        {
                            case "title":
                                item.Title = itemElement.InnerText;
                                break;
                            case "link":
                                item.Link = itemElement.InnerText;
                                break;
                            case "description":
                                item.Description = itemElement.InnerText;
                                break;
                            case "pubDate":
                                item.PubDate = itemElement.InnerText;
                                break;
                        }
                    }
                    list.Add(item);
                }
            }

            int count = 1;
            foreach (var element in list)
            {
                Console.WriteLine($"{count}. {element.Title}");
                Console.WriteLine($"{element.Link}\n");
                count++;
            }


            // Задание №2
            Student student = new Student
            {
                Id=1,
                FullName="Nikita Boltushkin",
                Age = 29,
                Gender="Man",
                Group="SEP-201",
                PhoneNumber="87773332211"
            };

            XmlDocument xmlStudent = new XmlDocument();

            XmlDeclaration xmlDeclaration = xmlStudent.CreateXmlDeclaration("1.0", "UTF-8", string.Empty);
            xmlStudent.AppendChild(xmlDeclaration);

            XmlElement rootElement = xmlStudent.CreateElement("student");
            xmlStudent.AppendChild(rootElement);

            XmlElement idElement = xmlStudent.CreateElement("id");
            idElement.InnerText = student.Id.ToString();
            rootElement.AppendChild(idElement);

            XmlElement fullNameElement = xmlStudent.CreateElement("fullName");
            fullNameElement.InnerText = student.FullName;
            rootElement.AppendChild(fullNameElement);

            XmlElement ageElement = xmlStudent.CreateElement("age");
            ageElement.InnerText = student.Age.ToString();
            rootElement.AppendChild(ageElement);

            XmlElement genderElement = xmlStudent.CreateElement("gender");
            genderElement.InnerText = student.Gender;
            rootElement.AppendChild(genderElement);

            XmlElement groupElement = xmlStudent.CreateElement("group");
            groupElement.InnerText = student.Group;
            rootElement.AppendChild(groupElement);

            XmlElement phoneNumberElement = xmlStudent.CreateElement("phoneNumber");
            phoneNumberElement.InnerText = student.PhoneNumber;
            rootElement.AppendChild(phoneNumberElement);

            xmlStudent.Save("student.xml");

            Console.ReadLine();
        }
    }
}
