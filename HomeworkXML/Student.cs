﻿
namespace HomeworkXML
{
    public class Student
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
        public string Group { get; set; }
        public string PhoneNumber { get; set; }
    }
}
